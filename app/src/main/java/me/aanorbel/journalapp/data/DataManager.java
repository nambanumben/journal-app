package me.aanorbel.journalapp.data;

import android.app.Activity;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.data.model.Journal;

public class DataManager {

    private static DataManager myObj;
    private static DatabaseReference mDatabase;

    private DataManager() {

    }

    public static DataManager getInstance() {
        if (myObj == null) {
            myObj = new DataManager();
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            firebaseDatabase.setPersistenceEnabled(true);
            mDatabase = firebaseDatabase.getReference();
        }
        return myObj;
    }


    public FirebaseRecyclerOptions<Journal> getListOptions(String url) {
        Query journalsQuery = mDatabase.child(url)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        return new FirebaseRecyclerOptions.Builder<Journal>()
                .setQuery(journalsQuery, Journal.class)
                .build();

    }

    public void saveJournal(Activity context, String title, String body) {
        String key = mDatabase.child(context.getString(R.string.firebase_user_journal_template,getUid())).push().getKey();
        Journal journal = new Journal(title, body);
        journal.setCreatedAt(new Date());
        Map<String, Object> journalValues = journal.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(context.getString(R.string.firebase_user_journal_item_template,getUid(),key), journalValues);

        mDatabase.updateChildren(childUpdates);

    }


    private String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
}
