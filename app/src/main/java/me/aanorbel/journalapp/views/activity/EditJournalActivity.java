package me.aanorbel.journalapp.views.activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.base.BaseActivity;
import me.aanorbel.journalapp.data.model.Journal;
import me.aanorbel.journalapp.presenters.EditJournalActivityPresenter;
import me.aanorbel.journalapp.presenters.EditJournalActivityPresenterImpl;
import me.aanorbel.journalapp.views.EditJournalActivityView;

public class EditJournalActivity extends BaseActivity implements EditJournalActivityView {

    public static final String EXTRA_JOURNAL_KEY = "EXTRA_JOURNAL_KEY";
    private String mJournalKey;
    private DatabaseReference mJournalReference;
    private EditText mTitleField;
    private EditText mBodyField;
    private ValueEventListener mJournalListener;
    private String TAG = EditJournalActivity.class.getSimpleName();
    private EditJournalActivityPresenter mPresenter;
    private Journal journal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_edit_journal);
        mJournalKey = getIntent().getStringExtra(EXTRA_JOURNAL_KEY);
        if (mJournalKey == null) {
            throw new IllegalArgumentException("Must pass EXTRA_JOURNAL_KEY");
        }

        mJournalReference = FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.firebase_user_journal_template, getUid())).child(mJournalKey);
        mPresenter = new EditJournalActivityPresenterImpl(mJournalReference);
        mPresenter.attachView(this);
        mBodyField = findViewById(R.id.et_body);
        mTitleField = findViewById(R.id.et_title);
    }


    @Override
    public void onStart() {
        super.onStart();
        ValueEventListener journalListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                journal = dataSnapshot.getValue(Journal.class);
                mTitleField.setText(journal.getTitle());
                mBodyField.setText(journal.getBody());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.w(TAG, "loadJournal:onCancelled", databaseError.toException());
                Toast.makeText(EditJournalActivity.this, "Failed to load journal.",
                        Toast.LENGTH_SHORT).show();
            }
        };
        mJournalReference.addValueEventListener(journalListener);

        mJournalListener = journalListener;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_journal, menu);
        for (int i = 0; i < menu.size(); i++) {
            Drawable yourdrawable = menu.getItem(i).getIcon(); // change 0 with 1,2 ...
            yourdrawable.mutate();
            yourdrawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_save) {
            submitJournal();
            return true;
        } else if (i == android.R.id.home) {
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    private void submitJournal() {
        final String title = mTitleField.getText().toString();
        final String body = mBodyField.getText().toString();

        // Title is required
        if (TextUtils.isEmpty(title)) {
            mTitleField.setError(getResources().getString(R.string.required));
            return;
        }

        if (TextUtils.isEmpty(body)) {
            mBodyField.setError(getResources().getString(R.string.required));
            return;
        }

        setEditingEnabled(false);
        Toast.makeText(this, "Saving...", Toast.LENGTH_SHORT).show();

        this.journal.setTitle(title);
        this.journal.setBody(body);
        mPresenter.submitJournal(journal, mJournalKey);

    }

    @Override
    public void onStop() {
        super.onStop();

        if (mJournalListener != null) {
            mJournalReference.removeEventListener(mJournalListener);
        }
    }

    private void setEditingEnabled(boolean enabled) {
        mTitleField.setEnabled(enabled);
        mBodyField.setEnabled(enabled);
    }
}
