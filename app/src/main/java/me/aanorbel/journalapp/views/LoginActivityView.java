package me.aanorbel.journalapp.views;

import com.google.firebase.auth.FirebaseUser;

import me.aanorbel.journalapp.base.BaseView;

public interface LoginActivityView extends BaseView {

    void loginSuccess(FirebaseUser user);

    void loginError();

}
