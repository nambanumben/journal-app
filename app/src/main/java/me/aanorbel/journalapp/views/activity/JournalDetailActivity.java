package me.aanorbel.journalapp.views.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.base.BaseActivity;
import me.aanorbel.journalapp.data.model.Journal;

public class JournalDetailActivity extends BaseActivity {

    public static final String EXTRA_JOURNAL_KEY = "EXTRA_JOURNAL_KEY";
    private static final String TAG = JournalDetailActivity.class.getSimpleName();
    private String mJournalKey;
    private DatabaseReference mJournalReference;
    private TextView mBodyView, mTitleView, mDateView;
    private ValueEventListener mJournalListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_journal_detail);
        mJournalKey = getIntent().getStringExtra(EXTRA_JOURNAL_KEY);

        if (mJournalKey == null) {
            throw new IllegalArgumentException("Must pass EXTRA_JOURNAL_KEY");
        }

        mJournalReference = FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.firebase_user_journal_template, getUid())).child(mJournalKey);

        mBodyView = findViewById(R.id.tv_body);
        mTitleView = findViewById(R.id.tv_title);
        mDateView = findViewById(R.id.tv_date);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        ValueEventListener journalListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Journal journal = dataSnapshot.getValue(Journal.class);
                mTitleView.setText(journal.getTitle());
                mBodyView.setText(journal.getBody());
                mDateView.setText(DateFormat.getDateInstance(DateFormat.SHORT)
                        .format(journal.getCreatedAt()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.w(TAG, "loadJournal:onCancelled", databaseError.toException());
                Toast.makeText(JournalDetailActivity.this, "Failed to load journal.",
                        Toast.LENGTH_SHORT).show();
            }
        };
        mJournalReference.addValueEventListener(journalListener);

        mJournalListener = journalListener;
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mJournalListener != null) {
            mJournalReference.removeEventListener(mJournalListener);
        }
    }

}
