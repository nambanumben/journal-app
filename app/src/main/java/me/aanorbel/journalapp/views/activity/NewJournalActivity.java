package me.aanorbel.journalapp.views.activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.base.BaseActivity;
import me.aanorbel.journalapp.presenters.NewJournalActivityPresenter;
import me.aanorbel.journalapp.presenters.NewJournalActivityPresenterImpl;
import me.aanorbel.journalapp.views.NewJournalActivityView;

public class NewJournalActivity extends BaseActivity implements NewJournalActivityView {

    private static final String TAG = NewJournalActivity.class.getSimpleName();
    private EditText mTitleField;
    private EditText mBodyField;
    private NewJournalActivityPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_new_journal);

        mTitleField = findViewById(R.id.et_title);
        mBodyField = findViewById(R.id.et_body);
        mPresenter = new NewJournalActivityPresenterImpl();
        mPresenter.attachView(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_journal, menu);
        for (int i = 0; i < menu.size(); i++) {
            Drawable yourdrawable = menu.getItem(i).getIcon(); // change 0 with 1,2 ...
            yourdrawable.mutate();
            yourdrawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_save) {
            submitJournal();
            return true;
        } else if (i == android.R.id.home) {
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void submitJournal() {
        final String title = mTitleField.getText().toString();
        final String body = mBodyField.getText().toString();

        if (TextUtils.isEmpty(title)) {
            mTitleField.setError(getResources().getString(R.string.required));
            return;
        }

        if (TextUtils.isEmpty(body)) {
            mBodyField.setError(getResources().getString(R.string.required));
            return;
        }

        setEditingEnabled(false);
        Toast.makeText(this, "Saving...", Toast.LENGTH_SHORT).show();

        mPresenter.submitJournal(title, body);

    }

    private void setEditingEnabled(boolean enabled) {
        mTitleField.setEnabled(enabled);
        mBodyField.setEnabled(enabled);
    }

}
