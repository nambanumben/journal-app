package me.aanorbel.journalapp.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.base.BaseActivity;
import me.aanorbel.journalapp.presenters.LoginActivityMvpPresenter;
import me.aanorbel.journalapp.presenters.LoginActivityPresenter;
import me.aanorbel.journalapp.views.LoginActivityView;

public class LoginActivity extends BaseActivity implements
        LoginActivityView, View.OnClickListener {

    public static final int RC_SIGN_IN = 5000;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private FirebaseAuth mAuth;

    private SignInButton mSignInButton;

    private LoginActivityMvpPresenter mMainPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSignInButton = findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        mMainPresenter = new LoginActivityPresenter(mAuth);

        mMainPresenter.attachView(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            loginSuccess(currentUser);
        } else {
            loginError();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMainPresenter.detachView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                mMainPresenter.fireBaseAuthWithGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
                Log.w(TAG, "Google sign in failed", e);
                loginError();
            }
        }
    }


    @Override
    public void onClick(View view) {
        int item = view.getId();
        if (item == R.id.sign_in_button) {
            mMainPresenter.signIn();
        }
    }

    @Override
    public void loginSuccess(FirebaseUser user) {
        Toast.makeText(this, "SignIn Sucessfull", Toast.LENGTH_SHORT).show();

        // Go to MainActivity
        startActivity(new Intent(this, JournalListActivity.class));
        finish();
    }

    @Override
    public void loginError() {
        Snackbar.make(findViewById(R.id.layout_login), "Login Failed", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", view -> mMainPresenter.signIn());
    }

}
