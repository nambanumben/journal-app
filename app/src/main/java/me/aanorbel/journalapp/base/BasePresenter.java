package me.aanorbel.journalapp.base;


import com.google.firebase.auth.FirebaseAuth;

public interface BasePresenter<V extends BaseView> {

    void attachView(V view);

    void detachView();

    default String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

}
