package me.aanorbel.journalapp.base;

import android.app.Activity;
import android.content.Context;

public interface BaseView {
    Activity getContext();
}