package me.aanorbel.journalapp.presenters;

import com.google.firebase.database.DatabaseReference;

import java.util.Date;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.data.model.Journal;
import me.aanorbel.journalapp.views.EditJournalActivityView;

public class EditJournalActivityPresenterImpl implements EditJournalActivityPresenter {
    private final DatabaseReference mDatabase;
    private EditJournalActivityView view;

    public EditJournalActivityPresenterImpl(DatabaseReference mDatabase) {
        this.mDatabase = mDatabase;
    }

    @Override
    public void attachView(EditJournalActivityView view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void submitJournal(Journal journal, String key) {
        String keyUrl = view.getContext().getString(R.string.firebase_user_journal_item_template, getUid(), key);
        journal.setUpdatedAt(new Date());
        mDatabase
                .setValue(journal);

        view.getContext().finish();
    }
}
