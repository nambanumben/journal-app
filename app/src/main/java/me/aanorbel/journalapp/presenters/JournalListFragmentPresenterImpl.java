package me.aanorbel.journalapp.presenters;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.google.firebase.database.DatabaseReference;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.data.DataManager;
import me.aanorbel.journalapp.views.JournalListFragmentView;
import me.aanorbel.journalapp.views.activity.EditJournalActivity;
import me.aanorbel.journalapp.views.activity.JournalDetailActivity;

public class JournalListFragmentPresenterImpl implements JournalListFragmentPresenter,
        JournalRecyclerAdapter.ItemClickListener {

    private JournalListFragmentView view;

    private RecyclerView mRecyclerView;
    private JournalRecyclerAdapter mAdapter;
    private DataManager mDataManager;

    public JournalListFragmentPresenterImpl() {
        mDataManager = DataManager.getInstance();
    }

    @Override
    public void attachView(JournalListFragmentView view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void onItemClickListener(String itemId) {
        Intent intent = new Intent(this.view.getContext(), JournalDetailActivity.class);
        intent.putExtra(JournalDetailActivity.EXTRA_JOURNAL_KEY, itemId);
        this.view.getContext().startActivity(intent);
    }

    @Override
    public void setUpRecyclerView(RecyclerView mRecyclerView, JournalRecyclerAdapter mAdapter) {
        this.mAdapter = mAdapter;
        this.mRecyclerView = mRecyclerView;
    }

    @Override
    public void displayList() {
        LinearLayoutManager mManager = new LinearLayoutManager(this.view.getContext());
        mRecyclerView.setLayoutManager(mManager);

        mAdapter = new JournalRecyclerAdapter(mDataManager.getListOptions(this.view.getContext()
                .getString(R.string.firebase_journal_ref)), this);

        mRecyclerView.setAdapter(mAdapter);

        new ItemTouchHelper(new SwipeCallback(this.view.getContext(), this.view.getContext()
                .getResources().getColor(R.color.colorSecondaryTint), ItemTouchHelper.LEFT) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                DatabaseReference databaseReference = mAdapter.getRef(position);
                databaseReference.removeValue();
            }
        }).attachToRecyclerView(mRecyclerView);

        new ItemTouchHelper(new SwipeCallback(this.view.getContext(),
                this.view.getContext()
                        .getResources().getColor(R.color.colorAccent), ItemTouchHelper.RIGHT) {

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {

                Intent intent = new Intent(view.getContext(), EditJournalActivity.class);
                intent.putExtra(EditJournalActivity.EXTRA_JOURNAL_KEY,
                        mAdapter.getRef(viewHolder.getAdapterPosition()).getKey());
                view.getContext().startActivity(intent);

            }
        }).attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void onStart() {
        if (mAdapter != null) {
            mAdapter.startListening();
        }
    }

    @Override
    public void onStop() {
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }
}
