package me.aanorbel.journalapp.presenters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;

import java.text.DateFormat;

import me.aanorbel.journalapp.R;
import me.aanorbel.journalapp.data.model.Journal;

public class JournalRecyclerAdapter extends FirebaseRecyclerAdapter<Journal, JournalRecyclerAdapter.JournalViewHolder> {

    private final ItemClickListener mItemClickListener;

    public JournalRecyclerAdapter(@NonNull FirebaseRecyclerOptions<Journal> options, ItemClickListener listener) {
        super(options);
        this.mItemClickListener = listener;
    }

    @Override
    protected void onBindViewHolder(@NonNull JournalViewHolder holder, int position, @NonNull Journal model) {
        holder.title.setText(model.getTitle());
        holder.date.setText(
                DateFormat.getDateInstance(DateFormat.SHORT)
                        .format(model.getCreatedAt()));

    }

    @NonNull
    @Override
    public JournalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        return new JournalViewHolder(inflater.inflate(R.layout.item_journal, viewGroup, false));
    }

    public interface ItemClickListener {
        void onItemClickListener(String itemId);
    }

    public class JournalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView title;
        public TextView date;

        public JournalViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_title);
            date = itemView.findViewById(R.id.tv_date);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            DatabaseReference databaseReference = getRef(getAdapterPosition());
            mItemClickListener.onItemClickListener(databaseReference.getKey());
        }
    }
}
