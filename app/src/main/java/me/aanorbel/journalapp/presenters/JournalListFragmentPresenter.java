package me.aanorbel.journalapp.presenters;

import android.support.v7.widget.RecyclerView;

import me.aanorbel.journalapp.base.BasePresenter;
import me.aanorbel.journalapp.views.JournalListFragmentView;

public interface JournalListFragmentPresenter extends BasePresenter<JournalListFragmentView>{
    void setUpRecyclerView(RecyclerView mRecyclerView, JournalRecyclerAdapter mAdapter);

    void displayList();

    void onStart();

    void onStop();
}
