package me.aanorbel.journalapp.presenters;

import me.aanorbel.journalapp.base.BasePresenter;
import me.aanorbel.journalapp.data.model.Journal;
import me.aanorbel.journalapp.views.EditJournalActivityView;

public interface EditJournalActivityPresenter extends BasePresenter<EditJournalActivityView> {
    void submitJournal(Journal journal, String mJournalKey);
}
