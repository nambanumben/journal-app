package me.aanorbel.journalapp.presenters;

import android.content.Intent;

import me.aanorbel.journalapp.views.JournalListActivityView;
import me.aanorbel.journalapp.views.activity.NewJournalActivity;

public class JournalListActivityPresenterImpl implements JournalListActivityPresenter {

    private JournalListActivityView view;

    @Override
    public void signOut() {

    }

    @Override
    public void newJournal() {
        this.view.getContext().startActivity(new Intent(this.view.getContext(), NewJournalActivity.class));
    }

    @Override
    public void attachView(JournalListActivityView view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
