# Journal App

[![Build Status](https://travis-ci.org/aanorbel/journal-app.svg?branch=master)](https://travis-ci.org/aanorbel/journal-app)

7 Days of Code Challenge ( `#7DaysofCodeChallenge` ) is an individual project/program completion task for the ALC learners. The goal is to
   * determine your overall progress in the ALC program, and identify the top 100 learners from this track
   * To test the skills learned by learners and certify the top learners at the end of the program.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites


### Android Studio
- Download and install latest Android Studio from https://developer.android.com/studio/index.html
- Open the downloaded project in Android Studio
- Install missing SDK and build-tools


## How to build apk
- Update the source code with `git pull --rebase`
- Run these following commands in a console: 
`cd JournalApp`
`./gradlew clean assembleDebug`
- The generated apk is located in `app/build/outputs/apk/debug`



## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
